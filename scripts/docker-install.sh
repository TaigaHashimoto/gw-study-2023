#! /bin/bash

# package install
sudo yum -y install vim git wget unzip

# docker remove
cat /etc/redhat-release
sudo yum -y update
sudo yum -y remove docker \
                docker-client \
                docker-client-latest \
                docker-common \
                docker-latest \
                docker-latest-logrotate \
                docker-logrotate \
                docker-engine

# docker install
sudo yum install -y yum-utils
sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

sudo yum -y install docker-ce docker-ce-cli containerd.io

docker --version

sudo systemctl start docker
sudo systemctl enable docker
sudo systemctl status docker

# docker-compose install
sudo curl -L https://github.com/docker/compose/releases/download/v2.16.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo usermod -aG docker $USER
docker-compose --version

echo "docker コマンドを使う場合は新しいターミナルでログインしなおしてください。"

###############################################################################
#
# When this script is finished, log in with the new tarminal.
#
###############################################################################
