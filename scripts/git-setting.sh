#! /bin/bash

mkdir ~/.ssh
cd ~/.ssh
ssh-keygen -f gitlab -t rsa -N ""

echo "---------"
cat ~/.ssh/gitlab.pub
echo "---------"

echo "上記の ----- 内をコピーして Gitlab の SSH Key の設定に貼り付ける"

touch ~/.ssh/config
echo "Host gitlab.com
  HostName gitlab.com
  User git
  IdentityFile ~/.ssh/gitlab
  IdentitiesOnly yes" >> ~/.ssh/config
sudo chmod 600 ~/.ssh/config

ssh -T git@gitlab.com
