# 2023年 GW 勉強会

## 環境構築
1. リポジトリを clone してくる
    ```shell
    git clone git@gitlab.com:TaigaHashimoto/gw-study-2023.git
    ```
2. docker をインストールする
    ```shell
    sh ./scripts/docker-install.sh
    ```
3. コンテナを起動する
    ```shell
    docker-compose up -d --build
    ```

## 環境の使い方
- mysql コンテナにログインする
    ```shell
    docker-compose exec mysql bash
    ```
- データベースにログインする
    ```sql
    myslq -u salto -p sakila --auto-rehash
    ```

## docker-compose コマンド
- コンテナを起動する
    ```shell
    docker-compose up -d
    ```
- コンテナのプロセスを確認する
    ```shell
    docker-compose ps -a
    ```
- コンテナのログを確認する
    ```shell
    docker-compose logs [コンテナ名]
    ```
- コンテナに入る
    ```shell
    docker-compose exec [コンテナ名] bash
    docker-compose exec [コンテナ名] sh
    ```
- コンテナを止める
    ```shell
    docker-compose stop [コンテナ名]
    ```
- 停止したコンテナを起動する
    ```shell
    docker-compose start [コンテナ名]
    ```
- コンテナを停止・削除する
    ```shell
    docker-compose down
    ```
- コンテナ・イメージ・ネットワーク・ボリュームを全削除する
    ```shell
    docker-compose down --rmi all --volumes --remove-orphans
    ```
