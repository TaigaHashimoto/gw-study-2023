include .env

init:
	docker-compose up -d --build

mysql:
	docker-compose exec mysql mysql -u salto -p$(MYSQL_PASSWORD) $(MYSQL_DATABASE) --auto-rehash

down:
	docker-compose down --rmi all --volumes --remove-orphans